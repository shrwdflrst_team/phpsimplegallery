<?php

function getImagesInDirectory($path)
{
	$pattern = sprintf("%s%s*.{%s}",
		$path,
		DIRECTORY_SEPARATOR,
		implode(",", getValidImageExtensions())
	);

    return glob($pattern, GLOB_BRACE);
}

function getValidImageExtensions()
{
    return array("png", "jpg", "jpeg", "gif");
}

function getFilenameFromPath($path)
{
    return pathinfo($path, PATHINFO_BASENAME);
}

function getTitleFromPath($path)
{
	return ucwords(
		preg_replace(
			"/[_|\-]+/i",
			" ",
			pathinfo($path, PATHINFO_FILENAME)
		)
	);
}

function titleToShortTitle($title, $length = 15)
{
	$shortTitle = substr($title, 0, $length);

    if(strlen($title) > $length) {
        $shortTitle .= "...";
    }
	return $shortTitle;
}

function createLinkFromPath($path)
{
    $path = getFilenameFromPath($path);
    $title = getTitleFromPath($path);
    $shortTitle = titleToShortTitle($title);

    return sprintf(
		"<a href='%s' title='%s'>%s</a>",
		$path,
		$title,
		$shortTitle
	);
}

$images = getImagesInDirectory(__DIR__);

?><!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Gallery</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style type="text/css">
            .no-js {
                display: none;
            }
            .js-enabled {
                display: block;
            }

            body {
                background: #242424;
                font-family: sans-serif;
                font-size: 0.8em;
                white-space: nowrap;
            }

            .links {
                display: inline-block;
                list-style: none;
                padding-left: 5px;
                margin: 15px;
                vertical-align: top;
            }

            .image {
                display: inline-block;
            }

            .links li {
                margin-top: 5px;
            }
            .links a,
            .links a:hover,
            .links a:active,
            .links a:visited {
                color: white;
            }

            .links a {
                text-decoration: none;
            }

            .links a:hover,
            .selected a,
            .selected a:visited,
            .selected a:active {
                color: #ff9900;
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <ul class="links">
            <?php foreach($images as $k => $imagePath): ?>
                <li class="<?php echo $k == 0 ? 'selected' : ''; ?>">
                    <?php echo createLinkFromPath($imagePath)?>
                </li>
            <?php endforeach;?>
        </ul>

        <?php if(is_array($images) && count($images) > 0): ?>
            <img class="image" src="<?php echo getFilenameFromPath($images[0]); ?>">
        <?php else :?>
            <div class="error">No images found</div>
        <?php endif; ?>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript">
        $(function() {
            var selected = window.location.hash.replace("#", "");

            selectImage(selected);
            $('.no-js').removeClass('no-js').addClass('js-enabled');

            $('.links a').each(function() {
                $(this).click(function(e) {
                    e.preventDefault();
                    selectImage($(this).attr('href'));
                });
            });

            function selectImage(src) {
            	if(src) {
                    $('.selected').removeClass('selected');
                    $('.image').attr('src', src);
                    $('a[href="' + src + '"]').closest('li').addClass('selected');
                    window.location.hash = src;
            	}
            	console.log(src);
            }
    	});
        </script>

    </body>
</html>